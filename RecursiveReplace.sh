read -p "How many things should be replaced? " amount

if expr "$amount" : '-\?[0-9]\+$' >/dev/null; then
    read -p "What should the starting path be? " startpath
    eval "startpath=$startpath"

    for ((i = 1 ; i <= $amount ; i++)); do
        read -p "($i/$amount): What do you want to replace? " match
        read -p "($i/$amount): What do you want to replace it with? " replacement
        # sudo grep -rl ./ | sudo xargs sed -i 's/$match/$replacement/g'
        sudo grep -rl "$startpath" | sudo xargs sed -i 's/$match/$replacement/g' # `sed -i 's/$match/$replacement/g' $(find . -type f)` works, too.
        echo ""
    done
else
    echo "ERROR: '$amount' is not an integer!"
    exit 1
fi
