script=`realpath $0`

read -p "What should the starting path be? " startpath
eval "startpath=$startpath"

read -p "What do you want to search for? " match
install -Dv /dev/null "${script%/*}/logs/found.txt"
sudo grep -inR --color=auto "$match" "$startpath" > "${script%/*}/logs/found.txt"
echo "Matches can be found in '${script%/*}/logs/found.txt'"
echo ""
